#!/bin/bash
for i in `seq 1 8`
do
    script=output/jobs/smooth.$i.sh
    cat << EOF > $script
#!/bin/bash
   
module load r
Rscript scripts/bsmooth.R $i
EOF

    sbatch -o $script.stdout -e $script.stderr --time 12:00:00 --cpus-per-task 1 --mem 64G --account def-masirard $script
done

